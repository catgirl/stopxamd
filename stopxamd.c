#define _POSIX_C_SOURCE 200809L
#define _GNU_SOURCE 100000000


#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>

int main(int argc, char* argv[]) {
	if(argc != 4) {
		fprintf(stderr, "usage: stopxamd <parking lot> <sleep time> <pid file>\n");
		exit(EXIT_FAILURE);
	}

	/* Parse sleep time */
	int sleep_time = 0;
	int res = sscanf(argv[2], "%d", &sleep_time);
	if(res <= 0 || sleep_time <= 0) {
		fprintf(stderr, "Error parsing sleep time, using default: 4s");
		sleep_time = 4;
	}

        /* Our process ID and Session ID */
        pid_t pid, sid;

        /* Fork off the parent process */
        pid = fork();
        if (pid < 0) {
		fprintf(stderr, "fork() failure");
                exit(EXIT_FAILURE);
        }
        if (pid > 0) {
                exit(EXIT_SUCCESS);
        }

        /* Change the file mode mask */
        umask(0);

        /* Create a new SID for the child process */
        sid = setsid();
        if (sid < 0) {
                exit(-1);
        }

        if ((chdir("/")) < 0) {
                exit(-2);
        }

	const char* pidfile = argv[3];
	int pidfd = open(pidfile, O_CREAT|O_WRONLY, 0644);
	if(pidfd < 0) {
		fprintf(stderr, "Error opening pid file: %s", pidfile);
		exit(-3);
	}
	pid_t child_pid = getpid();
	char pid_buf[32] = {0};
	int w = snprintf(pid_buf,32,"%ld",(long)child_pid);
	w = write(pidfd, pid_buf, w > 0 ? w : 0);
	if(w < 0) {
		fprintf(stderr, "Error writing pid to file");
		exit(-4);
	}

        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);


	const char* filename = argv[1];
	int fd;
	fd = open(filename, O_WRONLY|O_NOFOLLOW|O_SYNC|O_TRUNC);
	if(fd < 0) {
		exit(-5);
	}


	#define BUFSIZE 128
	char buf[BUFSIZE] = {0};

	unsigned counter = 0;

        while (1) {
		lseek(fd, 0, SEEK_SET);
		int wrt = snprintf(buf, BUFSIZE, "%u", counter);
		if(wrt > 0) write(fd, buf, wrt);
		++counter;
		sleep(sleep_time);
        }

   exit(EXIT_SUCCESS);
}
